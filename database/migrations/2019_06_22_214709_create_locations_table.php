<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->string('address', 200);
            $table->text('description')->nullable();
            $table->string('small_desc', 100)->nullable();
            $table->boolean('deleted');
            $table->string('link');
            $table->string('operation_hours')->nullable();
            $table->string('email');
            $table->bigInteger('user_creator_id');
            $table->bigInteger('zelo_location_tag_id');
            $table->boolean('enabled');
            $table->string('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
