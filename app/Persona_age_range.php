<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona_age_range extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'min', 'max',
    ];
}