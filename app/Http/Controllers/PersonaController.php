<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Location;
use App\Persona;
use App\Persona_age_range;
use App\Persona_attribute;
use Validator;

use App\Http\Controllers\BaseController as BaseController;


class PersonaController extends BaseController
{

    /**
     * Display a listing of the resource per location.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPersonasByLocation($location_id, Request $request)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){
            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }
            $personas = Persona::where('location_id', $location_id)->get();
            return $this->sendResponse($personas->toArray(), 'Lista de personas retornada com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $input = $request->all();
            $validator = Validator::make($input, [
                'name' => 'string|min:2|max:3',
                'location_id' => 'required',
            ]);

            $location_count = Location::where('id', $input['location_id'])->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona = new Persona();
            $persona->name = $input['name'];
            $persona->location_id = $input['location_id'];
            $persona->save();

            return $this->sendResponse($persona->toArray(), 'Persona criada com sucesso.');

        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Persona $persona)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $input = $request->all();
            $validator = Validator::make($input, [
                'name' => 'string|min:2|max:3',
            ]);

            $location_count = Location::where('id', $persona->location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona->name = $input['name'];
            $persona->save();

            return $this->sendResponse($persona->toArray(), 'Persona alterada com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $persona = Persona::where('id', $id)->first();

            $location_count = Location::where('id', $persona->location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }
        
            $persona->delete();

            $persona_age_range = Persona_age_range::where('persona_id', $id)->get();
            foreach ($persona_age_range as $age) {
                $age->delete();
            }
            $persona_attributes = Persona_attribute::where('persona_id', $id)->get();
            foreach ($persona_attributes as $attr) {
                $attr->delete();
            }
            return $this->sendResponse($persona->toArray(), 'Persona deletada com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Add a Age Range to the persona.
     *
     * @return \Illuminate\Http\Response
     */
    public function setPersonaAgeRange($location_id, Request $request)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $input = $request->all();
            $validator = Validator::make($input, [
                'persona_id' => 'required',
                'min' => 'required',
                'max' => 'required',
            ]);

            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_count = Persona::where('id', $input['persona_id'])->where('location_id', $location_id)->count();
            if($persona_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_age_range = Persona_age_range::where('persona_id', $input['persona_id'])->first();
            if(!$persona_age_range){
                $persona_age_range = new Persona_age_range();
                $persona_age_range->persona_id = $input['persona_id'];
            }
            $persona_age_range->min = $input['min'];
            $persona_age_range->max = $input['max'];
            $persona_age_range->save();
            return $this->sendResponse($persona_age_range->toArray(), 'Faixa etária cadastrada com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Return Age Range of the persona.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPersonaAgeRange($location_id, $persona_id)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_count = Persona::where('id', $persona_id)->where('location_id', $location_id)->count();
            if($persona_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_age_range = Persona_age_range::where('persona_id', $persona_id)->first();
            if($persona_age_range){
                return $this->sendResponse($persona_age_range->toArray(), 'Faixa etária retornada com sucesso.');
            }else{
                return $this->sendError('Faixa etária não encontrada.');
            }
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Add a Attribute to the persona.
     *
     * @return \Illuminate\Http\Response
     */
    public function addPersonaAttribute($location_id, Request $request)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $input = $request->all();
            $validator = Validator::make($input, [
                'persona_id' => 'required',
                'attribute_key ' => 'required',
                'description' => 'required',
            ]);

            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_count = Persona::where('id', $input['persona_id'])->where('location_id', $location_id)->count();
            if($persona_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_attribute = new Persona_attribute();
            $persona_attribute->persona_id = $input['persona_id'];
            $persona_attribute->attribute_key = $input['attribute_key'];
            $persona_attribute->description = $input['description'];
            $persona_attribute->save();
            return $this->sendResponse($persona_attribute->toArray(), 'Atributo cadastrado com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Edit a Attribute to the persona.
     *
     * @return \Illuminate\Http\Response
     */
    public function editPersonaAttribute($location_id, Request $request)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $input = $request->all();
            $validator = Validator::make($input, [
                'persona_attribute_id' => 'required',
                'persona_id' => 'required',
                'description' => 'required',
            ]);

            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_count = Persona::where('id', $input['persona_id'])->where('location_id', $location_id)->count();
            if($persona_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_attribute = Persona_attribute::where('id', $input['persona_attribute_id'])->where('persona_id', $input['persona_id'])->first();
            if($persona_attribute){
                $persona_attribute->description = $input['description'];
                $persona_attribute->save();
                return $this->sendResponse($persona_attribute->toArray(), 'Atributo cadastrado com sucesso.');
            }else{
                return $this->sendError('Atributo não encontrado.');
            }
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Delete a Attribute to the persona.
     *
     * @return \Illuminate\Http\Response
     */
    public function deletePersonaAttribute($location_id, $persona_id, $persona_attribute_id)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_count = Persona::where('id', $persona_id)->where('location_id', $location_id)->count();
            if($persona_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_attribute = Persona_attribute::where('id', $persona_attribute_id)->where('persona_id', $persona_id)->first();
            if($persona_attribute){
                $persona_attribute->delete();
                return $this->sendResponse($persona_attribute->toArray(), 'Atributo deletado com sucesso.');
            }else{
                return $this->sendError('Atributo não encontrado.');
            }
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Return Age Range of the persona.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPersonaAttributes($location_id, $persona_id)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_count = Persona::where('id', $persona_id)->where('location_id', $location_id)->count();
            if($persona_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $persona_attributes = Persona_attribute::where('persona_id', $persona_id)->get();
            if($persona_attributes->count()){
                return $this->sendResponse($persona_attributes->toArray(), 'Lista de atributos retornada com sucesso.');
            }else{
                return $this->sendError('Lista de atributos não encontrada.');
            }
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }
}
