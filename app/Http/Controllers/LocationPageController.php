<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Location_media_link;

class LocationPageController extends BaseController
{
    public function index($location_link){
    	$location = Location::where('link', $location_link)->first();
        $location_media_links = Location_media_link::where('location_id', $location->id)->get();
    	if($location){
	    	return view('location', [
	    		'location' => $location,
                'media_links' => $location_media_links,
                'media_keys' => config('enums.social_media.key_to_name'),
	    	]);
    	}else{
    		return null;
    	}
    }
}
