<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Location_media_link;
use Validator;

class MediaLinksController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLinksByLocation($location_id, Request $request)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){
            $location_count = Location::where('id', $location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }
            $media_links = Location_media_link::where('location_id', $location_id)->get();
            return $this->sendResponse($media_links->toArray(), 'Lista de links retornada com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $input = $request->all();
            $validator = Validator::make($input, [
                'media_key' => 'string|min:2|max:3',
                'link' => 'string|max:200',
                'location_id' => 'required',
            ]);

            $location_count = Location::where('id', $input['location_id'])->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $config_media_keys = config('enums.social_media.key_to_name');
            if(!isset($config_media_keys[$input['media_key']])){
                return $this->sendError('Media key não reconhecido.');
            }

            $location_media_link = new Location_media_link;
            $location_media_link->media_key = $input['media_key'];
            $location_media_link->link = $input['link'];
            $location_media_link->location_id = $input['location_id'];
            $location_media_link->save();

            $return['media_link'] = $location_media_link;
            return $this->sendResponse($return, 'Link criado com sucesso.');
        }
        return $this->sendError('Você não possui permissão pza essa ação.');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location_media_link $location_media_link)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $input = $request->all();
            $validator = Validator::make($input, [
                'link' => 'string|max:200',
            ]);

            $location_count = Location::where('id', $location_media_link->location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }

            $location_media_link->link = $input['link'];
            $location_media_link->save();

            $return['media_link'] = $location_media_link;
            return $this->sendResponse($return, 'Link alterado com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth('api')->user();
        if($user->permission_group == 2 || $user->permission_group == 1){

            $location_media_link = Location_media_link::where('id', $id)->first();

            $location_count = Location::where('id', $location_media_link->location_id)->where('user_creator_id', $user->id)->count();
            if($location_count == 0){
                return $this->sendError('Acesso negado.');
            }
        
            $location_media_link->delete();

            return $this->sendResponse($location_media_link->toArray(), 'Link deletado com sucesso.');
        }
        return $this->sendError('Você não possui permissão para essa ação.');
    }
}
