<?php
// $media_keys = config('enums.social_media.name_to_key')
// $media_keys['Facebook']
return [
    'social_media' => [
    	'name_to_key' => [
	        'Facebook' => 'fb',
	        'Instagram' => 'in',
	        'Twitter' => 'tw',
	        'LinkedIn' => 'li',
	        'YouTube' => 'yt',
	        'WhatsApp' => 'wp',
	    ],
	    'key_to_name' => [
	        'fb' => 'Facebook',
	        'in' => 'Instagram',
	        'tw' => 'Twitter',
	        'li' => 'LinkedIn',
	        'yt' => 'YouTube',
	        'wp' => 'WhatsApp',
	    ],
	],
	'persona_atribute_key' => [
		'general', 
		'pain', 
		'interest', 
		'ambition'
	]
];