<?php

use Illuminate\Http\Request;
use App\Http\Middleware\CheckIfUserIsEnabled;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'RegisterController@register');
Route::post('/obrigado', 'LandingPageController@store');


Route::middleware('auth:api')->group( function () {
	// Route::get('mailchimp/lists', 'MailChimpController@getLists')->middleware(CheckIfUserIsEnabled::class);
	// Route::get('mailchimp/campaigns', 'MailChimpController@getCampaigns')->middleware(CheckIfUserIsEnabled::class);
	// Route::get('mailchimp/automations', 'MailChimpController@getAutomations')->middleware(CheckIfUserIsEnabled::class);
	// Route::post('mailchimp/sendmail', 'MailChimpController@sendEmailTo')->middleware(CheckIfUserIsEnabled::class);
 //    Route::delete('delete/promotion/{id}', 'PromotionController@delete')->middleware(CheckIfUserIsEnabled::class);
 //    Route::put('enable/promotion/{id}', 'PromotionController@enable')->middleware(CheckIfUserIsEnabled::class);
	// Route::resource('promotions', 'PromotionController')->middleware(CheckIfUserIsEnabled::class);
 //    Route::resource('leads', 'LeadController')->middleware(CheckIfUserIsEnabled::class);
	// Route::put('promotions/update', 'PromotionController@updatePromo')->middleware(CheckIfUserIsEnabled::class);
 //    Route::get('promosale/reservations', 'PromotionController@getPromoSales')->middleware(CheckIfUserIsEnabled::class);
 //    Route::get('promosale/sold', 'PromotionController@getSoldPromoSales')->middleware(CheckIfUserIsEnabled::class);
 //    Route::post('promosale/setsold', 'PromotionController@setPromoSaleSold')->middleware(CheckIfUserIsEnabled::class);

    Route::resource('personas', 'PersonaController')->middleware(CheckIfUserIsEnabled::class);
    Route::get('get_location_personas/{location_id}', 'PersonaController@getPersonasByLocation')->middleware(CheckIfUserIsEnabled::class);
    Route::post('set_persona_age_range/{location_id}', 'PersonaController@setPersonaAgeRange')->middleware(CheckIfUserIsEnabled::class);
    Route::get('get_persona_age_range/{location_id}/{persona_id}', 'PersonaController@getPersonaAgeRange')->middleware(CheckIfUserIsEnabled::class);
    
    Route::post('add_persona_attribute/{location_id}/', 'PersonaController@addPersonaAttribute')->middleware(CheckIfUserIsEnabled::class);
    Route::post('edit_persona_attribute/{location_id}/', 'PersonaController@editPersonaAttribute')->middleware(CheckIfUserIsEnabled::class);
    Route::delete('delete_persona_attribute/{location_id}/{persona_id}/{persona_attribute_id}', 'PersonaController@deletePersonaAttribute')->middleware(CheckIfUserIsEnabled::class);
    Route::get('get_persona_attributes/{location_id}/{persona_id}', 'PersonaController@getPersonaAttributes')->middleware(CheckIfUserIsEnabled::class);

    Route::resource('locations', 'LocationController')->middleware(CheckIfUserIsEnabled::class);
    Route::delete('delete/location/{id}', 'LocationController@delete')->middleware(CheckIfUserIsEnabled::class);
    Route::put('enable/location/{id}', 'LocationController@enable')->middleware(CheckIfUserIsEnabled::class);
    Route::post('upload_image/location', 'LocationController@upload_image')->middleware(CheckIfUserIsEnabled::class);

    Route::resource('location_media_links', 'MediaLinksController')->middleware(CheckIfUserIsEnabled::class);
    Route::get('get_location_media_links/{location_id}', 'MediaLinksController@getLinksByLocation')->middleware(CheckIfUserIsEnabled::class);

    Route::get('currentuser', 'LocationController@getLoggedUser')->middleware(CheckIfUserIsEnabled::class);
});